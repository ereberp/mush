import { createApp } from 'vue'
import App from './App.vue'
import './assets/scss/main.scss';
import ApiService from "./services/api.service";
import {TokenService} from "./services/storage.service";
import store from './store'
import router from './router'

// Set the base URL of the API
ApiService.init(process.env.VUE_APP_API_URL);

// If token exists set header
if (TokenService.getToken()) {
    ApiService.setHeader()
}
// If 401 try to refresh token
ApiService.mount401Interceptor();


const app = createApp(App)

app.use(store)
app.use(router)

app.mount('#app');
