const ANDIE = "andie";
const CHAO = "chao";
const CHUN = "chun";
const DEREK = "derek";
const ELEESHA = "eleesha";
const FINOLA = "finola";
const FRIEDA = "frieda";
const GIOELE = "gioele";
const HUA = "hua";
const IAN = "ian";
const JANICE = "janice";
const JIN_SU = "kim_jin_su";
const KUAN_TI = "kuan_ti";
const PAOLA = "paola";
const RALUCA = "raluca";
const ROLAND = "roland";
const STEPHEN = "stephen";
const TERRENCE = "terrence";


export const characterEnum = {
    [ANDIE]: {
        'head': require('@/assets/images/char/head/andie.png'),
        'body': require('@/assets/images/char/body/andie.png'),
        'portrait': require('@/assets/images/char/portrait/andie_graham_portrait.jpg')
    },
    [CHAO]: {
        'head': require('@/assets/images/char/head/chao.png'),
        'body': require('@/assets/images/char/body/chao.png'),
        'portrait': require('@/assets/images/char/portrait/Wang_chao_portrait.jpg')
    },
    [CHUN]: {
        'head': require('@/assets/images/char/head/chun.png'),
        'body': require('@/assets/images/char/body/chun.png'),
        'portrait': require('@/assets/images/char/portrait/Zhong_chun_portrait.jpg')
    },
    [DEREK]: {
        'head': require('@/assets/images/char/head/derek.png'),
        'body': require('@/assets/images/char/body/derek.png'),
        'portrait': require('@/assets/images/char/portrait/derek_hogan_portrait.jpg')
    },
    [ELEESHA]: {
        'head': require('@/assets/images/char/head/eleesha.png'),
        'body': require('@/assets/images/char/body/eleesha.png'),
        'portrait': require('@/assets/images/char/portrait/Eleesha_williams_portrait.jpg')
    },
    [FINOLA]: {
        'head': require('@/assets/images/char/head/finola.png'),
        'body': require('@/assets/images/char/body/finola.png'),
        'portrait': require('@/assets/images/char/portrait/Finola_keegan_portrait.jpg')
    },
    [FRIEDA]: {
        'head': require('@/assets/images/char/head/frieda.png'),
        'body': require('@/assets/images/char/body/frieda.png'),
        'portrait': require('@/assets/images/char/portrait/Frieda_bergmann_portrait.jpg')
    },
    [GIOELE]: {
        'head': require('@/assets/images/char/head/gioele.png'),
        'body': require('@/assets/images/char/body/gioele.png'),
        'portrait': require('@/assets/images/char/portrait/Gioele_rinaldo_portrait.jpg')
    },
    [HUA]: {
        'head': require('@/assets/images/char/head/derek.png'),
        'body': require('@/assets/images/char/body/hua.png'),
        'portrait': require('@/assets/images/char/portrait/Jiang_hua_portrait.jpg')
    },
    [IAN]: {
        'head': require('@/assets/images/char/head/ian.png'),
        'body': require('@/assets/images/char/body/ian.png'),
        'portrait': require('@/assets/images/char/portrait/Ian_soulton_portrait.jpg')
    },
    [JANICE]: {
        'head': require('@/assets/images/char/head/derek.png'),
        'body': require('@/assets/images/char/body/janice.png'),
        'portrait': require('@/assets/images/char/portrait/Janice_kent_portrait.jpg')
    },
    [JIN_SU]: {
        'head': require('@/assets/images/char/head/derek.png'),
        'body': require('@/assets/images/char/body/jin_su.png'),
        'portrait': require('@/assets/images/char/portrait/Kim_jin_su_portrait.jpg')
    },
    [KUAN_TI]: {
        'head': require('@/assets/images/char/head/kuan_ti.png'),
        'body': require('@/assets/images/char/body/kuan_ti.png'),
        'portrait': require('@/assets/images/char/portrait/Lai_kuan_ti_portrait.jpg')
    },
    [PAOLA]: {
        'head': require('@/assets/images/char/head/derek.png'),
        'body': require('@/assets/images/char/body/paola.png'),
        'portrait': require('@/assets/images/char/portrait/Paola_rinaldo_portrait.jpg')
    },
    [RALUCA]: {
        'head': require('@/assets/images/char/head/derek.png'),
        'body': require('@/assets/images/char/body/raluca.png'),
        'portrait': require('@/assets/images/char/portrait/Raluca_tomescu_portrait.jpg')
    },
    [ROLAND]: {
        'head': require('@/assets/images/char/head/roland.png'),
        'body': require('@/assets/images/char/body/roland.png'),
        'portrait': require('@/assets/images/char/portrait/Roland_zuccali_portrait.jpg')
    },
    [STEPHEN]: {
        'head': require('@/assets/images/char/head/stephen.png'),
        'body': require('@/assets/images/char/body/stephen.png'),
        'portrait': require('@/assets/images/char/portrait/Stephen_seagull_portrait.jpg')
    },
    [TERRENCE]: {
        'head': require('@/assets/images/char/head/terrence.png'),
        'body': require('@/assets/images/char/body/terrence.png'),
        'portrait': require('@/assets/images/char/portrait/Terrence_archer_portrait.jpg')
    },
}