const HEAVY = "heavy";
const HIDDEN = "hidden";
const PLANT_YOUNG = "plant_young";
const CHARGE = "charges";


export const statusItemEnum = {
    [HEAVY]: {
        'icon': require('@/assets/images/status/heavy.png'),
    },
    [HIDDEN]: {
        'icon': require('@/assets/images/status/hidden.png'),
    },
    [PLANT_YOUNG]: {
        'icon': require('@/assets/images/status/plant_youngling.png'),
    },
    [CHARGE]: {
        'icon': require('@/assets/images/status/charge.png'),
    },
}