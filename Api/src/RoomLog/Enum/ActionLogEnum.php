<?php

namespace Mush\RoomLog\Enum;

class ActionLogEnum
{
    public const BUILD_SUCCESS = 'build_success';
    public const COFFEE_SUCCESS = 'coffee_success';
    public const CONSUME_SUCCESS = 'consume_success';
    public const COOK_SUCCESS = 'cook_success';
    public const DROP = 'drop';
    public const EXPRESS_COOK_SUCCESS = 'express_cook_success';
    public const EXTRACT_SPORE_SUCCESS = 'extract_spore_success';
    public const GET_UP = 'get_up';
    public const HIDE_SUCCESS = 'hide_success';
    public const HIT_SUCCESS = 'hit_success';
    public const HYPERFREEZE_SUCCESS = 'hyperfreeze_success';
    public const INFECT_SUCCESS = 'infect_success';
    public const LIE_DOWN = 'lie_down';
    public const EXIT_ROOM = 'exit_room';
    public const ENTER_ROOM = 'enter_room';
    public const READ_BOOK = 'read_book';
    public const READ_DOCUMENT = 'read_document';
    public const REPAIR_SUCCESS = 'repair_success';
    public const SEARCH_SUCCESS = 'search_success';
    public const SEARCH_FAIL = 'search_fail';
    public const SHRED_SUCCESS = 'shred_success';
    public const TAKE = 'take';
    public const TRANSPLANT_SUCCESS = 'transplant_success';
    public const TRANSPLANT_FAIL = 'transplant_fail';
    public const TREAT_PLANT_SUCCESS = 'treat_plant_success';
    public const WATER_PLANT_SUCCESS = 'water_plant_success';
    public const WRITE_SUCCESS = 'write_success';
}
