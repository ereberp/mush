<?php

namespace Mush\Room\Enum;

class RoomEnum
{
    public const BRIDGE = 'bridge';
    public const ALPHA_BAY = 'alpha_bay';
    public const BRAVO_BAY = 'bravo_bay';
    public const ALPHA_BAY_2 = 'alpha_bay_2';
    public const NEXUS = 'nexus';
    public const MEDLAB = 'medlab';
    public const LABORATORY = 'laboratory';
    public const REFECTORY = 'refectory';
    public const HYDROPONIC_GARDEN = 'hydroponic_garden';
    public const ENGINE_ROOM = 'engine_room';
    public const FRONT_ALPHA_TURRET = 'front_alpha_turret';
    public const CENTRE_ALPHA_TURRET = 'centre_alpha_turret';
    public const REAR_ALPHA_TURRET = 'rear_alpha_turret';
    public const FRONT_BRAVO_TURRET = 'front_bravo_turret';
    public const CENTRE_BRAVO_TURRET = 'centre_bravo_turret';
    public const REAR_BRAVO_TURRET = 'rear_bravo_turret';
    public const FRONT_CORRIDOR = 'front_corridor';
    public const CENTRAL_CORRIDOR = 'central_corridor';
    public const REAR_CORRIDOR = 'rear_corridor';
    public const PLANET = 'planet';
    public const ICARUS_BAY = 'icarus_bay';
    public const ALPHA_DORM = 'alpha_dorm';
    public const BRAVO_DORM = 'bravo_dorm';
    public const FRONT_STORAGE = 'front_storage';
    public const CENTER_ALPHA_STORAGE = 'center_alpha_storage';
    public const REAR_ALPHA_STORAGE = 'rear_alpha_storage';
    public const CENTER_BRAVO_STORAGE = 'center_bravo_storage';
    public const REAR_BRAVO_STORAGE = 'rear_bravo_storage';
    public const SPACE = 'space';
    public const GREAT_BEYOND = 'great_beyond';
}
